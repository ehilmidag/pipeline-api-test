package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUnit_MongoDBConnect(t *testing.T) {

	Convey("MongoDB Connect test", t, func() {
		client, ctx := MongoClient()

		So(ctx, ShouldNotBeNil)
		So(client, ShouldNotBeNil)
	})

}

func TestUnit_EmptyError(t *testing.T) {
	Convey("Empty model", t, func() {
		m := Model{}

		_, err := AddNote(&m)

		So(err, ShouldNotBeNil)

})
}
