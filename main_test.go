package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"testing"

	"github.com/gofiber/fiber/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAcceptance_NoteListGet(t *testing.T) {
	Convey("Given note model ", t, func() {

		m1 := Model{ID: "1", Note: "lorem ipsum"}
		m2 := Model{ID: "2", Note: "lorem hamza"}
		m3 := Model{ID: "3", Note: "lorem abbas"}

		AddNote(&m1)
		AddNote(&m2)
		AddNote(&m3)

		Convey("When I get request notes", func() {
			request, _ := http.NewRequest(http.MethodGet, "/notes", nil)

			request.Header.Add("Content-Type", "application/json")

			app := fiber.New()
			app = Endpoints(app)
			response, err := app.Test(request, 30000)
			So(err, ShouldBeNil)

			Convey("Then should return status code 200", func() {
				So(response.StatusCode, ShouldEqual, fiber.StatusOK)

				Convey("Then should return notes list", func() {
					responseBody, err := ioutil.ReadAll(response.Body)
					So(err, ShouldBeNil)

					returnedData := List{}

					err = json.Unmarshal(responseBody, &returnedData)
					So(err, ShouldBeNil)
					So(returnedData.Notes[0].ID, ShouldEqual, m1.ID)
					So(returnedData.Notes[1].ID, ShouldEqual, m2.ID)
					So(returnedData.Notes[2].ID, ShouldEqual, m3.ID)
					So(returnedData.Notes[0].Note, ShouldEqual, m1.Note)
					So(returnedData.Notes[1].Note, ShouldEqual, m2.Note)
					So(returnedData.Notes[2].Note, ShouldEqual, m3.Note)
				})
			})
		})

	})

}

func TestAcceptance_NotePost(t *testing.T) {
	Convey("Given add note body", t, func() {
		m := Model{ID: "1", Note: "lorem ipsum"}

		Convey("When i send a post req err should be empty", func() {
			noteByte, err := json.Marshal(m)
			So(err, ShouldBeNil)

			noteReader := bytes.NewReader(noteByte)
			req, err := http.NewRequest(http.MethodPost, "/notes", noteReader)
			So(err, ShouldBeNil)

			req.Header.Add("Content-Type", "application/json")
			req.Header.Set("Content-Length", strconv.Itoa(len(noteByte)))

			app := fiber.New()
			app = Endpoints(app)

			res, err := app.Test(req, 20000)
			So(err, ShouldBeNil)

			Convey("Then status code should be 201", func() {
				So(res.StatusCode, ShouldEqual, fiber.StatusCreated)
			})

			Convey("My response should be equal my note model", func() {
				returnedData := Model{}
				responseBody, _ := ioutil.ReadAll(res.Body)
				err = json.Unmarshal(responseBody, &returnedData)

				So(err, ShouldBeNil)
				So(returnedData.ID, ShouldEqual, m.ID)
				So(returnedData.Note, ShouldEqual, m.Note)
			})

			// preBody, _ := json.Marshal(&m)
			// body := bytes.NewReader(preBody)
			// request, _ := http.NewRequest(http.MethodPost, "/notes", body)

			// request.Header.Add("Content-Type", "application/json")

			// app := fiber.New()
			// app = Endpoints(app)

			// response, err := app.Test(request, 30000)
			// So(err, ShouldBeNil)

			// Convey("Response status should be 201", func() {
			// 	So(response.StatusCode, ShouldEqual, fiber.StatusCreated)
			// })
			// Convey("Then should return note ", func() {

			// 	returnedData := Model{}
			// 	responseBody, _ := ioutil.ReadAll(response.Body)
			// 	err = json.Unmarshal(responseBody, &returnedData)

			// 	So(err, ShouldBeNil)

			// 	So(returnedData.ID, ShouldEqual, m.ID)
			// 	So(returnedData.Note, ShouldEqual, m.Note)
			// })

		})

	})

}
