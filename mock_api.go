package main

func MockGetNotes() (*List, error) {

	m1 := Model{ID: "1", Note: "lorem ipsum", V: 0}
	m2 := Model{ID: "2", Note: "lorem hamza", V: 0}
	m3 := Model{ID: "3", Note: "lorem abbas", V: 0}

	emptyList := List{
		Notes: []Model{m1, m2, m3},
	}

	return &emptyList, nil
}

func MockAddNote() (*Model, error) {
	emptyModel := Model{ID: "1", Note: "lorem ipsum"}

	return &emptyModel, nil
}

// func GetNote(id string) *Model {
// 	client, ctx := MongoClient()
// 	db := client.Database("note").Collection("notes")

// 	emptyModel := Model{}

// 	db.FindOne(ctx, bson.M{"_id": id}).Decode(&emptyModel)

// 	return &emptyModel

// }
